package main

import (
	"fmt"
	"html/template"
	"path"
	"regexp"
	"strings"
	"time"

	"git.lan/darchiveviewer/datafmt"
	"github.com/bwmarrin/discordgo"
	"github.com/russross/blackfriday/v2"
)

const emojiBaseURL = "https://cdn.discordapp.com/emojis/"

var emojiMatch = regexp.MustCompile("<(?P<animated>a)?:(?P<name>\\w+):(?P<emoteID>\\d{15,})>")

var temfuncs = template.FuncMap{
	"formatTime": func(t time.Time) string {
		return t.Format("2006 Jan 02 15:04")
	},
	"seq": func(leng int, backwards bool) []int {
		seq := make([]int, leng)
		for i := 0; i < leng; i++ {
			if !backwards {
				seq[i] = i
			} else {
				seq[i] = leng - i - 1
			}
		}
		return seq
	},
	"sub": func(ints ...int) int {
		total := ints[0]
		for i, x := range ints {
			if i == 0 {
				continue // yes i am lazy shush
			}
			total -= x
		}
		return total
	},
	"dgAvatar": func(user datafmt.User) string {
		return discordgo.EndpointCDNAvatars + user.ID + "/" + user.Avatar + ".png?size=256"
	},
	"newBlankMsg": func() datafmt.Message {
		return datafmt.Message{}
	},
	"getFilename": func(s string) string {
		return path.Base(s)
	},
	"stringsHasSuffix": func(s string, p string) bool {
		return strings.HasSuffix(s, p)
	},
	"stringsHasSuffixAny": func(s string, p ...string) bool {
		//	fmt.Println(s, p)
		for _, x := range p {
			if strings.HasSuffix(s, x) {
				return true
			}
		}
		return false
	},
	"markdownParse": func(s string) template.HTML {
		return template.HTML(blackfriday.Run(
			[]byte(s),
			blackfriday.WithExtensions(blackfriday.Autolink|blackfriday.Strikethrough|blackfriday.HardLineBreak)))
	},
	"emojiToImgs": func(s string) string {
		emojis := emojiMatch.FindAllString(s, -1)
		if emojis == nil {
			return s
		}

		for _, e := range emojis {
			ext := ".png"
			groups := emojiMatch.FindStringSubmatch(e)
			if groups[1] == "a" {
				ext = ".gif"
			}
			name := groups[2]
			url := emojiBaseURL + groups[3] + ext

			s = strings.NewReplacer(e, fmt.Sprintf(`<img class="emoji" src="%v" alt="%v" />`, url, name)).Replace(s)
		}

		return s
	},
	"color": func(c int) string {
		return fmt.Sprintf("#%06X", c)
	},
}
