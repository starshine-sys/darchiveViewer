# DarchiveViewer

a prototype viewer/html generator for a custom discord channel archive format

currently just a prototype, currently doesn't support most of parts of the discord message object and the CSS and the HTML templates are at this point incomplete

## Usage
some basic instructions
1. Clone this repo to your computer and navigate to it with a command line/terminal

### Generating a file from a file
2. run the command
- `go run . -file <pathToFile>`

### Generating a file from a URL with the command line
2. run the command
-  `go run . -direct <urlToArchiveFile>`

### Web server
2. run the command
-  `go run . -web`

3. Navigate a browser to the address 
- `<host>/view?page=<urlToArchiveFile>`
- (host by default is `127.0.0.1:13120`)