module git.lan/darchiveviewer

go 1.14

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/russross/blackfriday/v2 v2.1.0
)
